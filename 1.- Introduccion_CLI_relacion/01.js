/*Este cuerpo es la forma de refereciar que generara un codigo con Vue.js */
const   app = new Vue({
    //el "#app" hace referencia a la id del div
    el: '#app',
    //los datos se trabajan atravez de data:
    data:{
        /*
        El nombre de esta etiqueta es generada por mi, osea que se pueden inventar etiquetas
        ademas de que este es referenciado dentro de la etiqueta <H1> dentro del HTML
        */
        titulo: 'Hola mundo con Vue.js',
        //generaremos un arreglo
        frutas: ['Manzana', 'Pera', 'Piña'],
        //Ahora trabajaremos con objetos
        venta_frutas: [
            {nombre: 'Manzana', cantidad: 10},
            {nombre: 'Pera', cantidad: 0},
            {nombre: 'Piña', cantidad: 7}
        ]
    }

})
